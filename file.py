'''
"r"   Opens a file for reading only.
"r+"  Opens a file for both reading and writing.
"rb"  Opens a file for reading only in binary format.
"rb+" Opens a file for both reading and writing in binary format.
"w"   Opens a file for writing only.
"a"   Open for writing.  The file is created if it does not exist.
"a+"  Open for reading and writing.  The file is created if it does not exist.
'''

#LECTURA
# \\ Windows
# / Linux

ruta = 'C:\\Archivo.txt'
ruta1 = 'C:\\Users\\fher_\\OneDrive\\Escritorio\\Archivo.txt'
archivo = open(ruta, 'r')
#print(archivo.read())
#print(archivo.readline())
#print(archivo.readline())
#print(archivo.readline())
print(archivo.readlines())
archivo.close()
print("Fin Lectura")

'''
#ESCRITURA
contenido = "----"
nueva_ruta = "C://Users//fher_//OneDrive//Escritorio//PyhtonSaeSap//Intermedio//ProyectosPython//pythonintermedio//ArchivoTest.txt"
archivo = open(nueva_ruta, 'w')
archivo.write(contenido)
archivo.close


archivo = open("C://Users//fher_//OneDrive//Escritorio//PyhtonSaeSap//Intermedio//ProyectosPython//pythonintermedio//ArchivoTest.txt","r")
#readline
for linea in archivo.readlines():
    print(linea)

archivo.close()
'''

#METODOS
def get_File(ruta, permiso):
    archivo = open(ruta, permiso)
    return archivo

def read_File(archivo):
    contenido = archivo.read()
    return contenido

def Write_File(archivo, texto):
    archivo.write(texto)

#TEST METODOS
arc = get_File("C://Users//fher_//OneDrive//Escritorio//PyhtonSaeSap//Intermedio//ProyectosPython//pythonintermedio//ArchivoTest.txt","r+")
print(read_File(arc))
Write_File(arc,"asd")
