# PythonIntermedio

## Crear una función
En Python, una función se define usando la palabra clave def

## Llamar a una función
Para llamar a una función, use el nombre de la función seguido de paréntesis

## Argumentos
La información se puede pasar a funciones como argumentos.

Los argumentos se especifican después del nombre de la función, entre paréntesis. Puede agregar tantos argumentos como desee, solo sepárelos con una coma.

El siguiente ejemplo tiene una función con un argumento (fname). Cuando se llama a la función, pasamos un nombre, que se usa dentro de la función para imprimir el nombre completo

## ¿Parámetros o argumentos?

Los términos parámetro y argumento se pueden usar para lo mismo: información que se pasa a una función.
Desde la perspectiva de una función:

Un parámetro es la variable que aparece entre paréntesis en la definición de la función.

Un argumento es el valor que se envía a la función cuando se llama.

## Número de argumentos
Por defecto, se debe llamar a una función con el número correcto de argumentos. Lo que significa que si su función espera 2 argumentos, debe llamar a la función con 2 argumentos, ni más ni menos.

## Argumentos arbitrarios, * argumentos
Si no sabe cuántos argumentos se pasarán a su función, agregue un *antes del nombre del parámetro en la definición de la función.

De esta forma, la función recibirá una tupla de argumentos y podrá acceder a los elementos en consecuencia

Los argumentos arbitrarios a menudo se acortan a * args en la documentación de Python.

## Argumentos de palabras clave
También puede enviar argumentos con la sintaxis clave = valor .

De esta manera, el orden de los argumentos no importa.

## Argumentos de palabras clave arbitrarias, ** kwargs
Si no sabe cuántos argumentos de palabras clave se pasarán a su función, agregue dos asteriscos: **antes del nombre del parámetro en la definición de la función.

De esta forma, la función recibirá un diccionario de argumentos y podrá acceder a los elementos en consecuencia:

## Pasar una lista como argumento
Puede enviar cualquier tipo de datos de argumento a una función (cadena, número, lista, diccionario, etc.), y se tratará como el mismo tipo de datos dentro de la función.

Por ejemplo, si envía una Lista como argumento, seguirá siendo una Lista cuando llegue a la función

## Valores devueltos
Para permitir que una función devuelva un valor, use la return declaración

## Python también acepta la recursividad de funciones, lo que significa que una función definida puede llamarse a sí misma.

La recursividad es un concepto matemático y de programación común. Significa que una función se llama a sí misma. Esto tiene la ventaja de significar que puede recorrer los datos para llegar a un resultado.

El desarrollador debe tener mucho cuidado con la recursividad, ya que puede ser bastante fácil escribir una función que nunca termina, o una que usa cantidades excesivas de memoria o potencia del procesador. Sin embargo, cuando se escribe correctamente, la recursividad puede ser un enfoque de programación muy eficiente y matemáticamente elegante.

En este ejemplo, tri_recursion () es una función que hemos definido para llamarse a sí misma ("recurse"). Usamos la variable k como dato, que disminuye ( -1 ) cada vez que recurrimos. La recursividad termina cuando la condición no es mayor que 0 (es decir, cuando es 0).

Para un nuevo desarrollador, puede tomar algún tiempo averiguar cómo funciona exactamente, la mejor manera de averiguarlo es probándolo y modificándolo.